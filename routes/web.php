<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/', 'HomepageController@wish');

Route::get('/wishes', 'HomepageController@wishes');

Auth::routes();

Route::get('/dashboard/{id?}', 'HomeController@index')->name('home');
Route::post('/dashboard/{id?}', 'HomeController@updateCreate')->name('home');
Route::get('/dashboard/delete/{id?}', 'HomeController@delete')->name('delete');
