## Birthday Wish Script

A noob friendly birthday wish script made for [Jagrit's] (https://www.facebook.com/Lies.Greed.Misery) birthday.

It's not a commercial project, so obviously this script sucks balls - be prepared for repetitive code, hard coding and ugly ass HTML that I wrote ages ago. It will make your eyes bleed on mobile devices. 

Also, I got cancer from the auto-play music. I can not do anything about it, I was asked to make this like that only.

If you're really interested in making this script better - fork it and send me a merge request or report issues. Otherwise, use it as -is and shut the fuck up. 

## Requirements
- PHP >= 5.6.4.
- OpenSSL PHP Extension.
- PDO PHP Extension.
- Mbstring PHP Extension.
- Tokenizer PHP Extension.
- XML PHP Extension.

## Installation and Getting Started

- `composer install`
- `cp .env.example .env`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed` to create admin user.
- Update `APP_URL` and `DB_*` values in `.env`
- Fire up php server with `php artisan serve` or `php -S localhost:8080 -t /Users/joshi/sites/birthday/public`
- Vist `/login` to access admin panel

Default login: 
`email@email.com:pa$$w0rd`

## License

Do What the Fuck You Want to Public License - [WTFPL](http://www.wtfpl.net/).
