<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

	return [
		'name' => 'Admin',
		'email' => 'email@email.com',
		'password' => $password ?: $password = bcrypt('pa$$w0rd'),
		'remember_token' => str_random(10),
	];
});


$factory->define(App\Wish::class, function (Faker\Generator $faker) {
	static $password;

	return [
		'name' => $faker->name,
		'wishes' => $faker->paragraph,
		'ip_address' => $faker->ipv4,
	];
});