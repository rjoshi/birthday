<?php

namespace App\Http\Controllers;

use App\Http\Requests\WishRequest;
use App\Wish;
use Illuminate\Http\Request;

class HomepageController extends Controller
{

	/**
	 * Wishes JSON
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function wishes()
	{
		return Wish::orderBy('created_at', 'DESC')->select(['name', 'wishes as wish'])->get();
	}

	public function wish(WishRequest $request)
	{
		$wish = new Wish;
		$wishMessage = 'Wish created successfully.';

		$wish->name = $request->name;
		$wish->wishes = $request->wish;
		$wish->ip_address = $request->ip();
		$wish->save();

		return redirect()->back()->with('wishCreated', $wishMessage);
	}
}
