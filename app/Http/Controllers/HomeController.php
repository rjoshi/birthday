<?php

namespace App\Http\Controllers;

use App\Http\Requests\WishRequest;
use App\Wish;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(Request $request)
    {
    	$wishes = Wish::orderBy('created_at', 'DESC')->paginate(20);

	    $wish = Wish::find($request->id);

        return view('home')->with('wishes', $wishes)->with('wish', $wish);
    }

	public function updateCreate( WishRequest $request )
	{
		$wish = Wish::find($request->id);
		$wishMessage = 'Wish updated successfully.';

		if(is_null($request->id)) {
			$wish = new Wish;
			$wishMessage = 'Wish created successfully.';
		}

		$wish->name = $request->name;
		$wish->wishes = $request->wish;
		$wish->ip_address = $request->ip();
		$wish->save();

		return redirect()->back()->with('wishCreated', $wishMessage);
    }

	public function delete(Request $request)
	{
		$wish = Wish::findOrFail($request->id);
		$wish->delete();
		return redirect()->back()->with('wishCreated', 'Delete wish id: ' . $wish->id);
	}

}
