var intervalState = null;
var defaultInterval = 4000;

function ticker(){
    $('.wishes ul li').first().appendTo($('.wishes ul')).slideUp().end().end().filter(':first-child').slideDown();
}

function wish(){
    $.getJSON('/wishes', function( data ) {
        var items = [];
        var loop = 0;
        var leech = data;
        $.each( leech, function( key, val) {
            $('.wishes ul').append('<li><p>'+val.wish+'</p><span>'+val.name+'</span></li>');
            loop++;
        });
    }).done(function(){
        $('.wishes ul li').hide().filter(':first').show();
        intervalState = setInterval(ticker, defaultInterval);
    });
}
$( document ).ready(function() {
    wish();
    var fblightbox = $('#fblightbox');
    fblightbox.css({'margin-left':'-'+(fblightbox.width()/2)+'px','margin-top':'-350px'});
    $(".wishlink").click(function() {
        $('.overlay').fadeIn();
        fblightbox.fadeIn();
    });
    $("#close").click(function() {
        $('.overlay').fadeOut();
        fblightbox.fadeOut();
    });

    $('body').css('height', $( window ).height());

    var player = document.getElementById("music-player");
    var audios = ['audio.mp3', 'audio2.mp3', 'audio3.mp3', 'audio4.mp3'];
    var seek = 0;
    player.src = audios[seek];
    player.volume = 0.25;
    player.play();

    player.onended = function() {
        seek++;
        if(audios.length - 1 < seek) { seek = 0; }
        player.src = audios[seek];
        player.play();
    };

    $('.wish-control').click(function () {
        $(this).find('i').toggleClass('fa fa-pause-circle fa fa-play-circle');

        var currentClass = $(this).find('i').attr('class');
        if(currentClass === 'fa fa-play-circle') {
            // Paused state
            clearInterval(intervalState);
        } else {
            // Play state
            intervalState = setInterval(ticker, defaultInterval);
        }
    });

    $('.audio-control').click(function () {
        $(this).find('i').toggleClass('fa fa-volume-off fa fa-volume-up');

        var currentClass = $(this).find('i').attr('class');
        if(currentClass === 'fa fa-volume-up') {
            // Paused state
            player.pause();
        } else {
            // Play state
            player.play();
        }
    });
});