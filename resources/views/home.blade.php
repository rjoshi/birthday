@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                {{--@isset($wish)--}}
                <div class="col-md-12">
                    <h3>{{isset($wish) ? 'Edit post: ' : 'Create Post'}} {{$wish->id ?? ''}}</h3>
                    @if(Session::has('wishCreated'))<div class="alert alert-success" role="alert">{{Session::get('wishCreated')}}</div>@endif
                    {!! Form::open(['url' => route('home', $wish->id ?? null)]) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @if ($errors->has('name')) has-error @endif">
                                <label for="name">Name</label>
                                {!! Form::text('name', old('name') ?? $wish->name ?? '', [ 'class' => 'form-control', 'placeholder' => 'Name']) !!}
                                @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                            </div>
                            <div class="form-group @if ($errors->has('wish')) has-error @endif">
                                <label for="wish">Wish</label>
                                {!! Form::textarea('wish', old('wish') ?? $wish->wishes ?? '', [ 'class' => 'form-control', 'placeholder' => 'Wish']) !!}
                                @if ($errors->has('wish')) <p class="help-block">{{ $errors->first('wish') }}</p> @endif
                            </div>

                            <button type="submit" class="btn btn-default">@if(isset($wish))Update Wish @else Create Wish @endif</button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
                {{--@endisset--}}


                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Wish</th>
                            <th>IP Address</th>
                            <th>Posted at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wishes as $wish)
                            <tr>
                                <th>{{$wish->id}}</th>
                                <th class="name-list">{{$wish->name}}</th>
                                <th>{{$wish->wishes}}</th>
                                <th>{{$wish->ip_address}}</th>
                                <th class="nam">@isset($wish->updated_at){{$wish->updated_at->format('d/m/y H:i:s')}}@endisset</th>
                                <th><a href="{{route('home', $wish->id)}}">Edit</a> <a href="{{route('delete', $wish->id)}}">Delete</a> </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <div class="col-md-12 text-right">
                        {{ $wishes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
