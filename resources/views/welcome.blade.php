<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Happy birthday Nox</title>
    <link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <meta name="robots" content="index,follow">
    <meta property="og:locale" content="en_US">
    <meta property="og:site_name" content="{{config('app.url')}}">
    <meta property="og:url" content="{{config('app.url')}}">
    <meta property="og:type" content="website">

    <meta property="og:title" content="Happy Birthday Nox">
    <meta property="og:description" content="3rd Aug, 2017">
    <meta property="og:image" content="http://i.imgur.com/4QxHqVz.jpg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="420">

</head>
<body>
<div class="wrap">
    <span class="wishlink">wish me</span>
    <div class="header">
        <h1 class="intro"></h1>
        <h2 class="about">A person with will of  a Tiger, Speed of Cheetah and Heart of Lion</h2>
    </div>
    <div class="wishes">
        <ul>

        </ul>
    </div>
    <div class="wrap-cake">
        <div class="cake">
            <div class="candle">
                <div class="flame"></div>
                <div class="flame"></div>
                <div class="flame"></div>
                <div class="flame"></div>
                <div class="flame"></div>
            </div>
            <div class="crust"></div>
            <div class="base"></div>
        </div>
    </div>
    <div class="controls">
        <span class="wish-control"><i class="fa fa-pause-circle" aria-hidden="true"></i></span>
        <span class="audio-control"><i class="fa fa-volume-off" aria-hidden="true"></i></span>
    </div>
</div>
<div id="fblightbox">
    <div class="fblightbox-wrap">
        <div class="fblightbox-content">
            <form action="/" method="POST">
                {!! csrf_field() !!}
                <div class="form-group">
                    {!! Form::text('name', old('name') ?? $wish->name ?? '', [ 'class' => 'form-control', 'placeholder' => 'Name', 'required', 'autocomplete' => 'off']) !!}
                </div>
                <div class="form-group">
                    {!! Form::textarea('wish', old('wish') ?? $wish->wishes ?? '', [ 'class' => 'form-control', 'placeholder' => 'Wish', 'rows' => 3, 'required']) !!}
                </div>
                <button type="submit" class="btn btn-default">Send Wishes</button>
            </form>
        </div>
        <a href="#" id="close" class="fbbutton">X</a>
    </div>
</div>
<div class="overlay"></div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="{{url('js/birthday.js')}}"></script>
<audio id="music-player"></audio>
</body>
</html>
